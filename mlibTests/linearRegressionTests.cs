﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mlib.Regression;

namespace mlib.Regression.Tests
{
    [TestClass()]
    public class linearRegressionTests
    {
        Vector<double> x = DenseVector.OfArray(new double[] { 10, 20, 30 });
  
        Matrix<double> X = DenseMatrix.OfArray(new double[,] {
                {0,30},
                {50,50},
                {100,80}});

        Vector<double> y = DenseVector.OfArray(new double[] { 0, 1, 2 });
        Vector<double> yy = DenseVector.OfArray(new double[] { -9.7699626167013776E-14, 0.99999999999988542, 1.9999999999998597 });

        Vector<double> exp = DenseVector.OfArray(new double[] { 2.2204460492503131E-16, 1, 1.9999999999999996 });

        [TestMethod()]
        public void linear_regression_fit_one_dim()
        {
            linearRegression model = new linearRegression();
            model.fit(x, y);
        }

        [TestMethod()]
        public void linear_regression_fit_two_dim()
        {
            linearRegression model = new linearRegression();
            model.fit(X, y);
        }

        [TestMethod()]
        public void linear_regression_predict_one_dim()
        {
            linearRegression model = new linearRegression();
            model.fit(x, y);
            Vector<double> res = model.predict(x);
            Assert.AreEqual(exp, res);
        }

        [TestMethod()]
        public void linear_regression_predict_two_dim()
        {
            linearRegression model = new linearRegression();
            model.fit(X, y);
            Vector<double> res = model.predict(X);
            Assert.AreEqual(res, yy);
        }
    }
}