﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using mlib.Classification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mlib.Classification.Tests
{
    [TestClass()]
    public class multiClassLogisticRegressionsTests
    {
        Vector<double> x = DenseVector.OfArray(new double[] { 20, 25, 33, 22, 34, 30, 35, 39});
        Vector<double> y = DenseVector.OfArray(new double[] { 0, 0, 0, 0, 1, 1, 1, 1});
        Vector<double> y_exp = DenseVector.OfArray(new double[] { 0, 0, 1, 0, 1, 1, 1, 1 });
        Vector<double> y_exp2 = DenseVector.OfArray(new double[] { 0, 0, 1, 0, 1, 0, 1, 1 });

        Matrix<double> X = DenseMatrix.OfArray(new double[,] {
                {1,50},
                {2,48},
                {15,16},
                {14,24},
                {46,14},
                {51,11}});

        Vector<double> Y = DenseVector.OfArray(new double[] { 0, 0, 1, 1, 2, 2 });

        [TestMethod(), Timeout(5000)]
        public void multi_class_logistic_regression_fit_one_dim()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression();
            model.fit(x, y);
        }

        [TestMethod()]
        public void multi_class_logistic_regression_fit_two_dim()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression();
            model.fit(X, Y);
        }

        [TestMethod(), Timeout(5000)]
        public void multi_class_logistic_regression_predict_one_dim()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression();
            model.fit(x, y);
            Vector<double> res = model.predict(x);
            Assert.AreEqual(res, y_exp2);
        }

        [TestMethod(), Timeout(5000)]
        public void multi_class_logistic_regression_predict_one_dim_stochastic()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(x, y);
            Vector<double> res = model.predict(x);
            Assert.AreEqual(res, y_exp);
        }

        [TestMethod()]
        public void multi_class_logistic_regression_predict_two_dim()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression();
            model.fit(X, Y);
            Vector<double> res = model.predict(X);
            Assert.AreEqual(res, Y);
        }

        [TestMethod()]
        public void multi_class_logistic_regression_predict_two_dim_stochastic()
        {
            multiClassLogisticRegression model = new multiClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(X, Y);
            Vector<double> res = model.predict(X);
            Assert.AreEqual(res, Y);
        }
    }
}