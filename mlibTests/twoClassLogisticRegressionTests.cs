﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace mlib.Classification.Tests
{
    [TestClass()]
    public class twoClassLogisticRegressionTests
    {
        Matrix<double> X = DenseMatrix.OfArray(new double[,] {
                {0,30},
                {50,40},
                {50,60},
                {100,80}});
        Vector<double> yy = DenseVector.OfArray(new double[] {0, 0, 1, 1});


        Vector<double> x = DenseVector.OfArray(new double[] { 20, 30 });
        Vector<double> y = DenseVector.OfArray(new double[] { 0, 1 });

        [TestMethod()]
        public void two_class_logistic_regression_fit_one_dim()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression();
            model.fit(x, y);
        }

        [TestMethod()]
        public void two_class_logistic_regression_fit_one_dim_stochastic()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(x, y);
        }

        [TestMethod()]
        public void two_class_logistic_regression_fit_two_dim()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression();
            model.fit(X, yy);
        }

        [TestMethod()]
        public void two_class_logistic_regression_fit_two_dim_stochastic()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(X, yy);
        }

        [TestMethod()]
        public void two_class_logistic_regression_predict_one_dim()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression();
            model.fit(x, y);
            Vector<double> res = model.predict(x);
            Assert.AreEqual(res, y);
        }

        [TestMethod()]
        public void two_class_logistic_regression_predict_one_dim_stochastic()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(x, y);
            Vector<double> res = model.predict(x);
            Assert.AreEqual(res, y);
        }

        [TestMethod()]
        public void two_class_logistic_regression_predict_two_dim()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression();
            model.fit(X, yy);
            Vector<double> res = model.predict(X);
            Assert.AreEqual(res, yy);
        }

        [TestMethod()]
        public void two_class_logistic_regression_predict_two_dim_stochastic()
        {
            twoClassLogisticRegression model = new twoClassLogisticRegression(algo: MinFunctions.minimizeFunctions.min_func.stochastic);
            model.fit(X, yy);
            Vector<double> res = model.predict(X);
            Assert.AreEqual(res, yy);
        }
    }
}