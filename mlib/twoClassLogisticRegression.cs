﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra.Double;
using mlib.MinFunctions;

namespace mlib.Classification
{
    public class twoClassLogisticRegression : model
    {
        private Vector<double> Theta;

        // Learning rate
        private double gamma;

        // Convergence stop
        private double precision;

        // Algorithm choose to minimize cost function
        private minimizeFunctions.min_func algo;


        /**
         * <sumary> Initialize model parameters. Gamma is the learning rate and algo is the algorithm to use for gradient descent. </sumary>
         */
        public twoClassLogisticRegression(double gamma = 0.01, double precision = 0.0001, minimizeFunctions.min_func algo = minimizeFunctions.min_func.batch)
        {
            this.gamma = gamma;
            this.precision = precision;
            this.algo = algo;
        }

        // Sigmoid function used to compute a prediction from datas
        private double sigmoid(Vector<double> x, Vector<double> myTheta)
        {
            return SpecialFunctions.Logistic((x.ToRowMatrix() * myTheta.ToColumnMatrix()).At(0, 0));
        }

        // Compute gradient derivation on the full set
        private Vector<double> dcost_full(Matrix<double> X, Vector<double> y, Vector<double> myTheta)
        {
            double m = X.RowCount;
            Vector<double> g = DenseVector.Create(myTheta.Count, 0);

            foreach (Tuple<int, Vector<double>> d in X.EnumerateRowsIndexed())
            {
                double p = sigmoid(d.Item2, myTheta);

                for (int i = 0; i < myTheta.Count; ++i)
                    g[i] += (p - y[d.Item1]) * d.Item2[i];
            }

            return g / m;
        }

        // Compute gradient derivation on one row
        private Vector<double> dcost(Vector<double> X, double y, Vector<double> myTheta)
        {
            Vector<double> g = DenseVector.Create(myTheta.Count, 0);

            double p = sigmoid(X, myTheta);

            for (int i = 0; i < myTheta.Count; ++i)
                g[i] += (p - y) * X[i];

            return g;
        }

        // Compute the cost of the predictiv function
        private double cost(Matrix<double> X, Vector<double> y)
        {
            double m = X.RowCount;
            double g = 0.0;

            foreach (Tuple<int, Vector<double>> d in X.EnumerateRowsIndexed())
            {
                double p = sigmoid(d.Item2, Theta);
                g += (Math.Log(p) * y[d.Item1] * -1) - ((1 - y[d.Item1]) * Math.Log(1 - p));
            }

            return (1.0 / m) * g;
        }

        private void fit_all(Matrix<double> X, Vector<double> y)
        {
            switch (algo)
            {
                case minimizeFunctions.min_func.stochastic:
                    Theta = minimizeFunctions.stochastic_gradient_descent(X, y, Theta, dcost, learning_rate: gamma, precision: precision);
                    break;
                case minimizeFunctions.min_func.batch:
                    Theta = minimizeFunctions.batch_gradient_descent(X, y, Theta, dcost_full, learning_rate: gamma, precision: precision);
                    break;
            }
        }

        /**
         * <sumary> Fit the model from a dataset with one dimentional datas. X are the examples and y the expected results </sumary>
         */
        public override void fit(Vector<double> X, Vector<double> y)
        {
            Matrix<double> datas = DenseMatrix.Create(X.Count, 1, 1).InsertColumn(1, X);
            Theta = DenseVector.Create(2, 0);
            fit_all(datas, y);
        }


        /**
         * <sumary> Fit the model from a dataset with multidimentionals datas. X are the examples and y the expected results </sumary>
         */
        public override void fit(Matrix<double> X, Vector<double> y)
        {
            Matrix<double> datas = X.InsertColumn(0, DenseVector.Create(X.RowCount, 1));
            Theta = DenseVector.Create(datas.ColumnCount, 0);
            fit_all(datas, y);
        }

        private Vector<double> makePred(Matrix<double> X)
        {
            Vector<double> res = DenseVector.Create(X.RowCount, 0);

            // Set the class of the row
            foreach (Tuple<int, Vector<double>> row in X.EnumerateRowsIndexed())
                res[row.Item1] = sigmoid(row.Item2, Theta) >= 0.5 ? 1 : 0;

            return res;
        }

        private Vector<double> makePred_prob(Matrix<double> X)
        {
            Vector<double> res = DenseVector.Create(X.RowCount, 0);
            foreach (Tuple<int, Vector<double>> row in X.EnumerateRowsIndexed())
                res[row.Item1] = sigmoid(row.Item2, Theta);
            return res;
        }

        /**
         * <sumary> Predict values from one dimention values </sumary>
         */
        public override Vector<double> predict(Vector<double> X)
        {
            Matrix<double> datas = DenseMatrix.Create(X.Count, 1, 1).InsertColumn(1, X);
            return makePred(datas);
        }

        /**
         * <sumary> Predict values from multi dimention values </sumary>
         */
        public override Vector<double> predict(Matrix<double> X)
        {
            Matrix<double> datas = X.InsertColumn(0, DenseVector.Create(X.RowCount, 1));
            return makePred(datas);
        }

        /**
         * <sumary> Predict probilities values from multi dimention values </sumary>
         */
        public Vector<double> predict_prob(Matrix<double> X)
        {
            Matrix<double> datas = X.InsertColumn(0, DenseVector.Create(X.RowCount, 1));
            return makePred_prob(datas);
        }

        /**
         * <sumary> Predict probilities values from one dimention values </sumary>
         */
        public Vector<double> predict_prob(Vector<double> X)
        {
            Matrix<double> datas = DenseMatrix.Create(X.Count, 1, 1).InsertColumn(1, X);
            return makePred_prob(datas);
        }
    }
}