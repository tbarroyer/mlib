﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using mlib.MinFunctions;

namespace mlib.Classification
{
    public class multiClassLogisticRegression : model
    {
        // Classifiers for one vs all
        private List<twoClassLogisticRegression> models;
        // List of all classes
        private List<double> classes;

        // Learning rate
        private double gamma;

        // Convergence stop
        private double precision;

        // Algorithm choose to minimize cost function
        private minimizeFunctions.min_func algo;

        public multiClassLogisticRegression(double gamma = 0.01, double precision = 0.0001, minimizeFunctions.min_func algo = minimizeFunctions.min_func.batch)
        {
            this.gamma = gamma;
            this.precision = precision;
            this.algo = algo;
        }

        private void init_models(Vector<double> y)
        {
            models = new List<twoClassLogisticRegression>();
            classes = new List<double>(y.Distinct<double>());
            int c = y.Distinct<double>().Count<double>();

            // Init all binary logistic models
            for (int i = 0; i < c; ++i)
                models.Add(new twoClassLogisticRegression(gamma: gamma, precision: precision, algo: algo));
        }

        public override void fit(Vector<double> X, Vector<double> y)
        {
            this.fit(X.ToColumnMatrix(), y);
        }

        public override void fit(Matrix<double> X, Vector<double> y)
        {
            init_models(y);

            // Fitting all models with the question is or is not a class
            for (int i = 0; i < classes.Count; ++i)
            {
                Vector<double> y_c = y.Clone();
                y_c = y_c.Map<double>(x => x == classes[i] ? 1.0 : 0.0);
                models[i].fit(X, y_c);
            }
        }

        public override Vector<double> predict(Vector<double> X)
        {
            return this.predict(X.ToColumnMatrix());
        }

        public override Vector<double> predict(Matrix<double> X)
        {
            // Predict 'is first class'
            Matrix<double> res_all = models[0].predict_prob(X).ToColumnMatrix();

            // Preduct 'is class' for all other classses
            for (int j = 1; j < models.Count; ++j)
                res_all = res_all.InsertColumn(j, models[j].predict_prob(X));

            // Store the final result with the predicted class of all rows
            Vector<double> res = DenseVector.Create(X.RowCount, 0);

            int i = 0;
            foreach (Vector<double> row in res_all.EnumerateRows())
            {
                // Set the result to the maximum probability 
                res[i] = classes[row.MaximumIndex()];
                ++i;
            }
            return res;
        }
    }
}