﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;

namespace mlib.Regression
{
    public class linearRegression : model
    {
        Vector<double> Theta;

        public override void fit(Vector<double> X, Vector<double> y)
        {
            // Add a 'one' column to get this form: a*1 + b*x1 + c*x2...
            Matrix<double> datas =  DenseMatrix.Create(X.Count, 1, 1).InsertColumn(1, X);
            // Normal equation, avoid gradient descent
            Theta = ((datas.Transpose() * datas).Inverse() * datas.Transpose() * y.ToColumnMatrix()).Column(0);
        }

        public override void fit(Matrix<double> X, Vector<double> y)
        {
            // Add a 'one' column to get this form: a*1 + b*x1 + c*x2...
            Matrix<double> datas = X.InsertColumn(0, DenseVector.Create(X.RowCount, 1));
            // Normal equation, avoid gradient descent
            Theta = ((datas.Transpose() * datas).Inverse() * datas.Transpose() * y.ToColumnMatrix()).Column(0);
        }

        public override Vector<double> predict(Vector<double> X)
        {
            // Add a 'one' column to get this form: a*1 + b*x
            Matrix<double> datas = DenseMatrix.Create(X.Count, 1, 1).InsertColumn(1, X);
            return datas * Theta;
        }

        public override Vector<double> predict(Matrix<double> X)
        {
            // Add a 'one' column to get this form: a*1 + b*x + c*x2 + ...
            Matrix<double> datas = X.InsertColumn(0, DenseVector.Create(X.RowCount, 1));
            return datas * Theta;
        }
    }
}