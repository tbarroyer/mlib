﻿using MathNet.Numerics.LinearAlgebra;

namespace mlib
{   
    public abstract class model
    {
        /** Constructor **/
        protected model()
        {

        }

        /** Method that make the model fitting datas **/
        public abstract void fit(Matrix<double> X, Vector<double> y);
        public abstract void fit(Vector<double> X, Vector<double> y);


        /** Methods that make the model predict values **/
        public abstract Vector<double> predict(Matrix<double> X);
        public abstract Vector<double> predict(Vector<double> X);
    }
}