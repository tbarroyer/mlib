﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace mlib.MinFunctions
{
    public static class minimizeFunctions
    {
        public enum min_func
        {
            batch,
            stochastic
        }

        private static void shuffle_datas(Matrix<double> X, Vector<double> y)
        {
            Matrix<double> y_c = y.ToColumnMatrix();

            Random x = new Random();
            for (int i = 0; i < X.RowCount; i += 2)
            {
                int p = x.Next(X.RowCount);

                // FIXME merge datas to avoid twice operations.
                Vector<double> row = X.Row(i);
                X.RemoveRow(i);
                X.InsertRow(p, row);

                Vector<double> res = y_c.Row(i);
                y_c.RemoveRow(i);
                y_c.InsertRow(p, res);
            }

            y_c.Column(0).CopyTo(y);
        }

        // Apply an update of the gradient for all examples (slow)
        public static Vector<double> batch_gradient_descent(Matrix<double> X, Vector<double> y, Vector<double> Theta,
                Func<Matrix<double>, Vector<double>, Vector<double>, Vector<double>> dcost,
                double learning_rate = 0.01, double precision = 0.0001)
        {
            // theta is keep because learning stop when abs(theta(0)-theta(1)) < precision
            Vector<double> Theta_old;

            // delta is keep for mementum optimisation
            Vector<double> old_delta = DenseVector.Create(Theta.Count, 0);

            do
            {
                Theta_old = Theta;

                Vector<double> delta = 0.9 * old_delta + learning_rate * dcost(X, y, Theta);
                Theta -= delta;

                old_delta = delta;

            } while (!Theta_old.ForAll2<double>((o, n) => Math.Abs(n - o) < precision, Theta));

            return Theta;
        }

        // Apply an update of the gradient foreach example (fast but overshooting)
        public static Vector<double> stochastic_gradient_descent(Matrix<double> X, Vector<double> y, Vector<double> Theta,
                Func<Vector<double>, double, Vector<double>, Vector<double>> dcost,
                double learning_rate = 0.01, double precision = 0.0001)
        {
            // theta is keep because learning stop when abs(theta(0)-theta(1)) < precision
            Vector<double> Theta_old;

            // delta is keep for mementum optimisation
            Vector<double> old_delta = DenseVector.Create(Theta.Count, 0);

            // g is and accumator to get delta on all datas
            Vector<double> g = DenseVector.Create(Theta.Count, 0);

            Random r = new Random();

            do
            {
                g = DenseVector.Create(Theta.Count, 0);

                Theta_old = Theta;

                // FIXME: need to find an optimize way to shuffle datas to get a better convergence
                // shuffle_datas(X_c, y_c);
                
                foreach (Tuple<int, Vector<double>> row in X.EnumerateRowsIndexed())
                {
                    // 0.9 * old_delta is the Momentum optimisation of the SGD
                    Vector<double> delta = 0.9 * old_delta + learning_rate * dcost(row.Item2, y[row.Item1], Theta);
                    Theta -= delta;
                    g += delta;
                }

                g = g / X.RowCount;
                old_delta = g;

            } while (!Theta_old.ForAll2<double>((o, n) => Math.Abs(n - o) < precision, Theta));

            return Theta;
        }
    }
}